provider "vault" {
    skip_tls_verify = true
}

data "vault_generic_secret" "vmware" {
    path ="secrets/vmware"
}

provider "vsphere" {
    user = "${data.vault_generic_secret.vmware.data["username"]}"
    password = "${data.vault_generic_secret.vmware.data["password"]}"
    vsphere_server = "${data.vault_generic_secret.vmware.data["server"]}"
    allow_unverified_ssl = true
}

data "vsphere_datacenter" "dc" {
    name = "${data.vault_generic_secret.vmware.data["datacenter"]}"
}

data "vsphere_datastore" "datastore" {
    name = "${data.vault_generic_secret.vmware.data["esx_datastore"]}"
    datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_compute_cluster" "cluster" {
    name = "${data.vault_generic_secret.vmware.data["cluster"]}"
    datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_network" "network" {
    name = "VM Network"
    datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_virtual_machine" "template" {
    name = "packer_windows2019"
    datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

resource "vsphere_virtual_machine" "windows" {
    wait_for_guest_net_timeout = 45
    name = "windows01"
    resource_pool_id = "${data.vsphere_compute_cluster.cluster.resource_pool_id}"
    datastore_id = "${data.vsphere_datastore.datastore.id}"

    num_cpus = 4
    memory = 8192
    guest_id = "${data.vsphere_virtual_machine.template.guest_id}"
    scsi_type = "${data.vsphere_virtual_machine.template.scsi_type}"

    enable_logging = true

    network_interface {
      network_id = "${data.vsphere_network.network.id}"
      adapter_type = "${data.vsphere_virtual_machine.template.network_interface_types[0]}"
    }

    disk {
        label = "disk0"
        size = "${data.vsphere_virtual_machine.template.disks.0.size}"
        eagerly_scrub = "${data.vsphere_virtual_machine.template.disks.0.eagerly_scrub}"
        thin_provisioned = "${data.vsphere_virtual_machine.template.disks.0.thin_provisioned}"
    }

    clone {
        template_uuid = "${data.vsphere_virtual_machine.template.id}"

        customize {
          timeout = 0
          windows_options {
              computer_name = "windows01"
          }

          network_interface {
          }
        }
    }
}

output "final" {
    value = "${formatlist("%v - %v", vsphere_virtual_machine.windows.*.default_ip_address, vsphere_virtual_machine.windows.*.name)}"
}