local "vcenter_username" {
    expression = vault("/secrets/data/vmware", "username")
    sensitive = true
}

local "vcenter_password" {
    expression = vault("/secrets/data/vmware", "password")
    sensitive = true
}

local "vcenter_server" {
    expression = vault("/secrets/data/vmware", "server")
    sensitive = true
}

local "vcenter_cluster" {
    expression = vault("/secrets/data/vmware", "cluster")
    sensitive = true
}

local "vcenter_datacenter" {
    expression = vault("/secrets/data/vmware", "datacenter")
    sensitive = true
}

local "esx_datastore" {
    expression = vault("/secrets/data/vmware", "esx_datastore")
    sensitive = true
}

local "esx_host" {
    expression = vault("/secrets/data/vmware", "esx_host")
    sensitive = true
}

locals {
    buildtime = formatdate("YYYY-MM-DD hh:mm ZZZ", timestamp())
}

source "vsphere-iso" "windows2019" {
    vcenter_server = local.vcenter_server
    username = local.vcenter_username
    password = local.vcenter_password
    cluster = local.vcenter_cluster
    datacenter = local.vcenter_datacenter
    datastore = local.esx_datastore
    host = local.esx_host
    folder ="Templates"
    insecure_connection = "true"

    notes = "Built by Packer on ${local.buildtime}"
    vm_name = "packer_windows2019"
    winrm_username = "Administrator"
    winrm_password = "S3cret!"
    CPUs = "4"
    RAM = "8192"
    RAM_reserve_all = true
    communicator = "winrm"
    disk_controller_type = ["lsilogic-sas"]
    firmware = "bios"
    floppy_files = [
        "artifacts/autounattend.xml",
        "artifacts/setup.ps1",
        "artifacts/winrm.bat",
        "artifacts/vmtools.cmd"
    ]
    guest_os_type = "windows9Server64Guest"
    iso_paths = [
        "[${local.esx_datastore}] ISO/Server2019_Eval.iso",
        "[] /vmimages/tools-isoimages/windows.iso"
    ]

    network_adapters {
        network = "VM Network"
        network_card = "vmxnet3"
    }

    storage {
        disk_size = "32768"
        disk_thin_provisioned = true
    }

    convert_to_template = true

    http_port_max = 8600
    http_port_min = 8600
}

build {
    sources = ["source.vsphere-iso.windows2019"]
}