local "proxmox_username" {
    expression = vault("/secrets/data/proxmox", "username")
    sensitive = true
}
local "proxmox_password" {
    expression = vault("/secrets/data/proxmox", "password")
    sensitive = true
}
local "proxmox_url" {
    expression = vault("/secrets/data/proxmox", "url")
    sensitive = true
}
local "proxmox_node" {
    expression = vault("/secrets/data/proxmox", "node")
    sensitive = true
}

source "proxmox" "ubuntu22-madalynn" {
  boot_command = ["c", "linux /casper/vmlinuz --- autoinstall ds='nocloud-net;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}/' ", "<enter><wait>", "initrd /casper/initrd<enter><wait>", "boot<enter>"]
  boot_wait    = "5s"
//   disks {
//     disk_size         = "20G"
//     storage_pool      = "local-lvm"
//     storage_pool_type = "lvm"
//     type              = "scsi"
//   }
    disks {
        type = "scsi"
        disk_size = "40G"
        storage_pool = "local-lvm"
        storage_pool_type = "lvm-thin"
        format = "raw"
    }
  http_directory = "http"
  iso_file       = "local:iso/ubuntu-22.04-live-server-amd64.iso"
  memory         = 8192
  network_adapters {
    bridge = "vmbr0"
  }
//   node          = "proxmox"
  node = local.proxmox_node

//   proxmox_url   = "https://proxmox.madalynn.xyz/api2/json"
  proxmox_url = local.proxmox_url
  ssh_password  = "madalynn"
  ssh_timeout   = "20m"
  ssh_username  = "madalynn"
  template_name = "ubuntu-22.04"
  unmount_iso   = true
//   username      = "${var.proxmox_username}"
  username = local.proxmox_username
//   password      = "${var.proxmox_password}"
  password = local.proxmox_password
}

build {
  sources = ["source.proxmox.ubuntu22-madalynn"]

}
