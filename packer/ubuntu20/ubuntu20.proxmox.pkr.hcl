local "proxmox_username" {
    expression = vault("/secrets/data/proxmox", "username")
    sensitive = true
}
local "proxmox_password" {
    expression = vault("/secrets/data/proxmox", "password")
    sensitive = true
}
local "proxmox_url" {
    expression = vault("/secrets/data/proxmox", "url")
    sensitive = true
}
local "proxmox_node" {
    expression = vault("/secrets/data/proxmox", "node")
    sensitive = true
}

locals {
    # for storing the build time of the image so we can see it at deployment
    buildtime = formatdate("YYYY-MM-DD hh:mm ZZZ", timestamp())
}


source "proxmox-iso" "ubuntu20" {
    # type = "proxmox"
    proxmox_url = local.proxmox_url

    username = local.proxmox_username
    password = local.proxmox_password
    insecure_skip_tls_verify = "true" # seems like a bad idea but with a self-signed cert, packer won't be able to connect otherwise
    node = local.proxmox_node

    qemu_agent = true
    unmount_iso = true
    os = "l26"
    # template_name = "packer_ubuntu20" template_name must be a valid DNS name ... wtf
    vm_name = "packer-ubuntu20"
    template_name = "packer-ubuntu20"
    template_description = "Built by Packer on ${local.buildtime}"
    
    sockets = "1"
    cores = "4"
    memory = "8192"

    scsi_controller = "virtio-scsi-pci"   
    disks {
        type = "scsi"
        disk_size = "40G"
        storage_pool = "local-lvm"
        storage_pool_type = "lvm-thin"
        format = "raw"
    }

    network_adapters {
        bridge = "vmbr0"
    }

    iso_file = "backups:iso/ubuntu-20.04.3-live-server-amd64.iso"
    iso_checksum = "none"

    boot_wait = "4s"
    # boot_command = [
    #     "<esc><esc><esc>",
    #     "<enter><wait>",
    #     "/casper/vmlinuz ",
    #     "root=/dev/sr0 ",
    #     "initrd=/casper/initrd ",
    #     "autoinstall ",
    #     "ds=nocloud-net;s=http://10.10.30.80:8600/",
    #     "<enter>"
    # ]

    # "autoinstall ds=nocloud-net;seedfrom=http://{{ .HTTPIP }}:{{ .HTTPPort }}/", # jenkins does NOT like this when running in a container
    boot_command = [
        "<enter><enter><f6><esc><wait> ",
        "autoinstall ds=nocloud-net;seedfrom=http://10.10.30.80:{{ .HTTPPort }}/",
        "<enter><wait>"
    ]
    
    ssh_password = "ubuntu"
    ssh_username = "ubuntu"
    ssh_timeout = "30m"
    ssh_handshake_attempts = "100"
    communicator = "ssh"

    # shutdown_command = "sudo -S -E shutdown -P now"
    # shutdown_timeout = "15m"
    http_port_min = 8600
    http_port_max = 8610
    http_directory = "./artifacts"
}

build {
    sources = ["source.proxmox-iso.ubuntu20"]

    provisioner "shell" {
        inline = [
            # this does NOT seem to run???? installing qemu-guest-agent in cloud-init template for now
            # this will run AFTER everything is done. No reboot after this so the qemu-guest-agent will not be running YET
            "echo %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%",
            "echo Running updates",
            "sudo apt-get update",
            # "sudo apt-get -y install open-vm-tools",
            "sudo apt-get -y install qemu-guest-agent",
            "sudo touch /etc/cloud/cloud-init.disabled", # Fixing issues with preboot DHCP
            "sudo apt-get -y purge cloud-init",
            "sudo sed -i \"s/D /tmp 1777/#D /tmp 1777/\" /usr/lib/tmpfiles.d/tmp.conf",
            "sudo sed -i \"s/After=/After=dbus.service /\" /lib/systemd/system/open-vm-tools.service",
            "sudo rm -rf /etc/machine-id", # next four lines fix same ip address being assigned in vmware
            "sudo rm -rf /var/lib/dbus/machine-id",
            "sudo touch /etc/machine-id",
            "sudo ln -s /etc/machine-id /var/lib/dbus/machine-id",
            "echo %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%",
        ]
    }
}